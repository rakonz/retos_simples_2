import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:retos_simples_2/pages/home_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent));
      return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Reto simple 2',
      initialRoute: 'home',
      routes: {
      'home': (_) => HomePage()
      }
    );
  }
}