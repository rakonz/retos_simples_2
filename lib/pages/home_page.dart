import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xffECF0F3),
        body: SingleChildScrollView(
          child: Column(
            children: [
              _FirstCard(size: size), 
              _SecondCard(size: size)
            ],
          ),
        ),
      ),
    );
  }
}

class _SecondCard extends StatelessWidget {
  const _SecondCard({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        height: 340,
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, 4.0),
                blurRadius: 6.0),
          ],
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: 20, left: size.width * 0.05, right: size.width * 0.05),
              child: _AlarmSwitch(),
            ),
            Divider(
                color: Color(0xffCECECE),
                height: 32,
                indent: size.width * 0.05,
                endIndent: size.width * 0.05),
            Padding(
              padding: EdgeInsets.only(top: 10, left: size.width * 0.05),
              child: _ToBed(size: size),
            ),
            Padding(
              padding: EdgeInsets.only(left: size.width * 0.04),
              child: _VerticalDivider(
                height: 20,
                color: Color(0xffCECECE),
              ),
            ),
            _HoursSleep(size: size),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: EdgeInsets.only(left: size.width * 0.04),
              child: _VerticalDivider(
                height: 20,
                color: Color(0xffCECECE),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: size.width * 0.05),
              child: _WakeUp(size: size),
            ),
          ],
        ),
      ),
    );
  }
}

class _AlarmSwitch extends StatelessWidget {
  const _AlarmSwitch({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "alarm",
          style: TextStyle(
              color: Color(0xff676F76), fontWeight: FontWeight.w400),
        ),
        SizedBox(width: 217),
        _SwitchButton()
      ],
    );
  }
}

class _WakeUp extends StatelessWidget {
  const _WakeUp({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.light_mode,
          size: 36,
        ),
        SizedBox(width: size.width * 0.05),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "07:30",
              style: TextStyle(
                  fontSize: 32, fontWeight: FontWeight.w500),
            ),
            Text("wake up",
                style:
                    TextStyle(color: Color(0xff676F76), fontSize: 16))
          ],
        )
      ],
    );
  }
}

class _HoursSleep extends StatelessWidget {
  const _HoursSleep({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 10, left: size.width * 0.06),
          child: Row(
            children: [
              Icon(
                Icons.notifications_paused,
                color: Colors.grey,
              ),
              SizedBox(
                width: size.width * 0.05,
              ),
              Column(
                children: [
                  Text(
                    "8 hours sleep",
                    style: TextStyle(color: Color(0xff676F76), fontSize: 16),
                  )
                ],
              )
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: size.width * 0.05),
          child: Icon(Icons.chevron_right),
        )
      ],
    );
  }
}

class _ToBed extends StatelessWidget {
  const _ToBed({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          Icons.dark_mode,
          size: 36,
        ),
        SizedBox(
          width: size.width * 0.05,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "to bed",
              style: TextStyle(color: Color(0xff676F76), fontSize: 16),
            ),
            Text(
              "23:30",
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 32),
            )
          ],
        )
      ],
    );
  }
}

class _VerticalDivider extends StatelessWidget {
  final Color color;
  final double height;

  const _VerticalDivider({
    required this.height,
    required this.color,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: Container(
        height: height,
        alignment: Alignment.bottomLeft,
        child: VerticalDivider(
          thickness: 1,
          color: color,
          width: 2,
        ),
      ),
    );
  }
}

class _SwitchButton extends StatelessWidget {
  const _SwitchButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(29),
      child: Container(
        color: Color(0xff242B32),
        height: 35,
        width: 55,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
                padding: EdgeInsets.only(left: 2),
                child: Text("on",
                    style: TextStyle(
                        color: Color(0xff676F76),
                        fontWeight: FontWeight.w400))),
            ClipRRect(
              borderRadius: BorderRadius.circular(29),
              child: Container(
                width: 20,
                height: 20,
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _FirstCard extends StatelessWidget {
  const _FirstCard({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
      child: Container(
        width: double.infinity,
        height: size.height * 0.25,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 4.0),
                  blurRadius: 6.0)
            ],
            gradient: LinearGradient(
              begin: Alignment(-1, 0.9),
              end: Alignment(-1, -0.85),
              colors: [
                // Color(0xFFFFDC76), // yellow sun
                Color(0xFFFFDC76), // yellow sun
                Color(0xFFFAACA2), // blue sky
                Color(0xFFE8C7F4), // blue sky
                Color(0xFFCDF1FE), // blue sky
              ],
            )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Padding(
                padding: EdgeInsets.only(
                  right: size.height * 0.02,
                  top: size.height * 0.01,
                ),
                child: Icon(Icons.close_outlined)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 22),
                  child: Text(
                    "You did it! Here \nyou can manage \nyour alarm,\nchange time and \nother things.",
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  child: Image.asset("assets/d.png"),
                  height: 120,
                  width: 190,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
